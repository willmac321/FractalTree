# FractalTree
Creates a fractal tree of 16 branches that randomly changes color.

![](https://gitlab.com/willmac321/FractalTree/raw/master/FractalTree.gif)
